import React, { Component } from 'react';
// import welcomeIcon from './assets/welcome.jpg';

import { provideContext } from './context';

import Panels from './Panels';
import Welcome from './Welcome';
import Messages from './Messages';

import { Wrapper } from './styled/app';

class App extends Component {
  render() {
    return (
      // <img src={welcomeIcon} alt="Welcome!"/>
      <Wrapper>
        <Panels />
        <Welcome />
        <Messages />
      </Wrapper>
    );
  }
}

export default provideContext(App);
