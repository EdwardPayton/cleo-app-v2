import React from 'react';
import { shallow } from 'enzyme';

import Messages from '.';

describe('<Messages />', () => {
  let wrapper;

  it('Renders without crashing', () => {
    wrapper = shallow(<Messages />);
    expect(wrapper).toHaveLength(1);
  });
});
