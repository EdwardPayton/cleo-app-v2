import React from 'react';
import { consumeContext } from '../context';

import { Text } from '../styled';
import { Message } from '../styled/messages';

const Messages = props => {
  const {
    message,
  } = props;

  return (
    <Message hidden={!message}>
      <Text small>{message}</Text>
    </Message>
  )
}

export default consumeContext(Messages);
