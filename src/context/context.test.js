import React from 'react';
import { mount, shallow } from 'enzyme';

import { provideContext, consumeContext } from '.';

class MockComponent extends React.Component {
  render() {
    return (
      <p>Test component</p>
    );
  }
} 

describe('provideContext HOC', () => {
  let wrapper, HocComponent, instance;

  beforeEach(function() {
    HocComponent = provideContext(MockComponent);
    wrapper = mount(<HocComponent />);
    instance = wrapper.instance();
  });

  it('Renders without crashing', () => {
    expect(wrapper).toHaveLength(1);
  });

});
