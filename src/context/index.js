import React, { Component } from 'react';
import { API_URL, TABS_CONFIG } from '../constants';

const { Provider, Consumer } = React.createContext({});

const provideContext = WrappedComponent => {
  return class extends Component {
    constructor(props) {
      super(props);

      this.state = {
        api: {
          apiLoading: true,
          apiError: false,

          bills: [],
          categories: [],
        },

        processed: {
          bills: [],
          potential: [],
        },

        selected: {
          category: -1,
          tab: 0,
        },

        message: null,
      };

      this.actions = {
        handleSelectionChange: this.handleSelectionChange,
        handleBillChange: this.handleBillChange
      };
    }

    componentDidMount() {
      this.fetchNewData();
    }

    fetchNewData() {
      const fetchBills = fetch(`${API_URL}/bills`)
      .then(response => response.json())
      .then(bills => {
        this.dispatchAction('api', {bills})
        return bills;
      })
      .then(bills => this.processBills(bills))

    const fetchCats = fetch(`${API_URL}/categories`)
      .then(response => response.json())
      .then(categories => {
        categories.unshift({name:'All', id:-1})
        this.dispatchAction('api', {categories})
      })

    Promise.all([fetchBills,fetchCats])
      .then(() => {
        this.dispatchAction('api', {apiLoading: false});
        this.showMessage('Loaded all your bills');
      })
      .catch(() => {
        this.dispatchAction('api', {apiError: true});
        this.showMessage('There was a problem getting your data', null);
      });
    }

    dispatchAction(section, newState) {
      const currentState = this.state[section];
      this.setState({
        [section]: {
          ...currentState, 
          ...newState
        }
      });
    }

    processBills(json = {}) {
      const processed = {
        bills: [],
        potential: [],
      };
  
      json.forEach(item => {
        if (item.isBill) return processed.bills.push(item);
        return processed.potential.push(item);
      });

      return this.setState({processed});
    }

    handleBillChange = itemId => {
      const currentState = this.state.api;
      const { bills } = currentState;
      const itemToAmend = bills.find(bill => bill.id === itemId);

      this.dispatchAction('api', {apiLoading: true});
  
      fetch(`${API_URL}/bills/${itemId}`, {
        method: 'PATCH',
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({isBill: !itemToAmend.isBill}),
      })
      .then(response => {
        this.dispatchAction('api', {apiLoading: false});
        response.status === 200 
          ? this.showMessage('Bills updated')
          : this.showMessage('There was a problem updating the bill', null);
        return response;
      })
      .then(response => response.json())
      .then(updated => {
        const updatedBillIndex = bills.findIndex(bill => bill.name === updated.name);
        bills[updatedBillIndex] = updated;
        this.processBills(bills);
      })
      .catch(error => console.log('Update error', error));
    }

    handleSelectionChange = (name, value) => {
      this.dispatchAction('selected', {[name]: value});
    }

    showMessage(message, time = 1500) {
      if (!time) return this.setState({message});

      this.setState({message}, () => {
        setTimeout(() => {
          this.setState({message: null});
        }, time);
      });
    }

    render() {
      const value = { ...TABS_CONFIG, ...this.state, actions: this.actions };
      return (
        <Provider value={value} >
          <WrappedComponent />
        </Provider>
      );
    }
  }
};

const consumeContext = WrappedComponent => props => {
  return (
    <Consumer>
      {value => <WrappedComponent {...props} {...value} />}
    </Consumer>
  );
};

export { provideContext, consumeContext };
