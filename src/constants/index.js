export const API_URL = 'http://localhost:3002';

export const STYLES = {
  colors: {
    blue: '#0815FF',
    yellow: '#F7BF31',
    yellowDark: '#b08416',
    charcoal: '#212b31',
    red: '#FF1744',
    green: '#00ab47'
  },
  font: {
    sizeS: '15px',
    sizeM: '20px',
    sizeL: '30px',
  },
};

export const ASSETS = {
  loader: require('../assets/loader.gif'),
  welcome: require('../assets/welcome.jpg'),
  cleoCoin: require('../assets/cleo_coin.jpg'),
};

export const TABS_CONFIG = {
  tabs: [
    { id: 0, name: 'Bills', actionName: 'Remove bill' },
    { id: 1, name: 'Potential', actionName: 'Add as bill' },
  ],
};
