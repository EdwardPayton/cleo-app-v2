import React from 'react';
import { consumeContext } from '../context';

import Categories from '../Categories';
import PanelButtons from './PanelButtons';
import Panel from './Panel';

import { PanelWrap } from '../styled/panels';

const Panels = props => {
  const {
    api: { bills },
    tabs,
    selected,
  } = props;

  return (
    <PanelWrap index={selected.tab}>
      {bills.length > 0 && (
        <React.Fragment>
          <PanelButtons />
          <Categories />
          {tabs.map(tab => <Panel tab={tab} key={tab.id} />)}
        </React.Fragment>
      )}
    </PanelWrap>
  );
}

export default consumeContext(Panels);
