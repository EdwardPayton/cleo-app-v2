import React from 'react';
import { consumeContext } from '../context';

import Bills from '../Bills';

import { PanelInner } from '../styled/panels';

const Panel = props => { 
  const {
    tab: { name, id, actionName },
    selected: { tab }
  } = props;
  
  return (
    <PanelInner hidden={id !== tab}>
      <Bills type={name.toLowerCase()} actionName={actionName} />
    </PanelInner>
  );
}

export default consumeContext(Panel);
