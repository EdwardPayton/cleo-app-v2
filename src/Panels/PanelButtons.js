import React from 'react';
import { consumeContext } from '../context';

import { Button, Span } from '../styled';
import { PanelBtns, PanelBtn } from '../styled/panels';

const PanelButtons = props => {
  const {
    actions,
    tabs,
    selected: { tab }
  } = props;

  const onClick = e => {
    const btnId = e.currentTarget.id.replace('tab', '');
    actions.handleSelectionChange('tab', parseInt(btnId, 10));
  }

  return (
    <PanelBtns>
      {tabs.map(({name, id}) => (
        <PanelBtn key={id} active={tab === id}>
          <Button onClick={onClick} type="button" id={'tab'+id}>
            <Span large>{name}</Span>
          </Button>
        </PanelBtn>
      ))}
    </PanelBtns>
  );
}

export default consumeContext(PanelButtons);
