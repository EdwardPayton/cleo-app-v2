import React from 'react';
import { shallow } from 'enzyme';

import Panels from '.';

describe('<Panels />', () => {
  let wrapper;

  it('Renders without crashing', () => {
    wrapper = shallow(<Panels />);
    expect(wrapper).toHaveLength(1);
  });
});
