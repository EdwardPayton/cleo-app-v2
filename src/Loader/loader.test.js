import React from 'react';
import { shallow } from 'enzyme';

import Loader from '.';

describe('<Loader />', () => {
  let wrapper;

  it('Renders without crashing', () => {
    wrapper = shallow(<Loader />);
    expect(wrapper).toHaveLength(1);
  });
});
