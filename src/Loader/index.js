import React from 'react';
import { consumeContext } from '../context';

import { ASSETS } from '../constants';

const Loader = props => {
  return (
    <div>
      <img src={ASSETS.loader} alt="" />
    </div>
  );
};

export default consumeContext(Loader);
