import React from 'react';
import { shallow } from 'enzyme';

import Welcome from '.';

describe('<Welcome />', () => {
  let wrapper;

  it('Renders without crashing', () => {
    wrapper = shallow(<Welcome />);
    expect(wrapper).toHaveLength(1);
  });
});
