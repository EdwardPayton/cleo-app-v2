import React from "react";
import { consumeContext } from "../context";

import { ASSETS } from '../constants';

import { Text } from "../styled";
import { WelcomeWrapper, WelcomeInner } from "../styled/welcome";

const Welcome = props => {
  const {
    api: { bills }
  } = props;

  return (
    <WelcomeWrapper hidden={bills.length}>
      <WelcomeInner>
        <Text>Welcome!</Text>
        <img src={ASSETS.welcome} alt="" />
      </WelcomeInner>
    </WelcomeWrapper>
  );
};

export default consumeContext(Welcome);
