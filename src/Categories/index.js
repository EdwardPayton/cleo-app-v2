import React from 'react';
import { consumeContext } from '../context';

import { Span } from '../styled';
import { CatWrapper, CatInputWrap, CatInput, CatLabel } from '../styled/categories';

const Categories = props => {
  const {
    actions,
    api: { categories },
    selected: { category },
  } = props;

  const handleSelectionChange = e => {
    let value;
    const { type } = e.currentTarget;
    if (type === 'radio') value = e.currentTarget.id.replace('radio', '');
    else value = e.currentTarget.value;
    const newCategory = parseInt(value, 10);
    actions.handleSelectionChange('category', newCategory);
  }

  return (
    <CatWrapper>
      <div>
        {categories.map(({id, name, iconUrl}) =>
          <CatInputWrap key={id} active={category === id}>
            <CatInput
              type="radio"
              id={'radio' + id}
              name="category"
              checked={category === id}
              onChange={handleSelectionChange}
              value={name}
            />
            <CatLabel htmlFor={'radio' + id}>
              <Span small>{name}</Span>
              {iconUrl && <img src={iconUrl} alt="" style={{width: '20px', height: '20px'}} />}
            </CatLabel>
          </CatInputWrap>
        )}
      </div>
    </CatWrapper>
  );
}

export default consumeContext(Categories);
