import React from 'react';
import { shallow } from 'enzyme';

import Categories from '.';

describe('<Categories />', () => {
  let wrapper;

  it('Renders without crashing', () => {
    wrapper = shallow(<Categories />);
    expect(wrapper).toHaveLength(1);
  });
});
