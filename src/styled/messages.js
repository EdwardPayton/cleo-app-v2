import styled from 'styled-components';

import { STYLES } from '../constants';
import { slideUp } from './_keyframes';

export const Message = styled.div`
  position: fixed;
  bottom: 20px;
  left: 20px;
  padding: 15px;
  min-width: 300px;
  border-radius: 4px;
  background: ${STYLES.colors.charcoal};
  color: #fff;
  box-shadow: 0 5px 10px 0 rgba(0,0,0,0.2);
  dispay: ${props => props.hidden ? 'none' : 'block'};
  animation: ${slideUp} .25s ease 1 forwards;
`;
