import { keyframes } from 'styled-components';

export const opacity = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

export const slideUp = keyframes`
  from {
    opacity: 0;
    transform: translateY(100px);
  }
  to {
    opacity: 1;
    transform: translateY(0);
  }
`;

export const maxHeight = keyframes`
from {
  opacity: 0;
  max-height: 0;
}
to {
  opacity: 1;
  max-height: 300px;
}
`;