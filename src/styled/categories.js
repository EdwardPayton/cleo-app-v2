import styled from 'styled-components';

// import { STYLES } from '../constants';

export const CatWrapper = styled.div`
  width: 800px;
  padding: 10px 0;
  margin: auto;
`;

export const CatInputWrap = styled.div`
  display: inline-block;
  border-bottom: 2px solid;
  padding: 5px 0;
  margin-right: 15px;
  border-bottom-color: ${props => props.active ? '#fff' : 'transparent' };
  transition:
    border-color .3s ease;
`;

export const CatInput = styled.input`
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
`;

export const CatLabel = styled.label`
  cursor: pointer;
`;