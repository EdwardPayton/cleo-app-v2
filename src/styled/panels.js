import styled from 'styled-components';

import { STYLES } from '../constants';

// Panels
export const PanelWrap = styled.div`
  height: calc(100% - 100px);
  padding: 50px 30px;
  background-color: ${props => props.index === 0 ? STYLES.colors.blue : STYLES.colors.yellowDark};
  overflow: auto;
  transition:
    background-color .2s ease;
`;

// Panel
export const PanelInner = styled.div`
  dispay: ${props => props.hidden ? 'none' : 'block'};
  width: 800px;
  margin: auto;
`;

// PanelButtons
export const PanelBtns = styled.div`
  width: 800px;
  margin: auto;
`;

export const PanelBtn = styled.div`
  display: inline-block;
  margin-right: 15px;
  border-bottom: 2px solid;
  border-color: ${props => props.active ? 'white' : 'transparent'};
  transition:
    border-color .3s ease;
`;