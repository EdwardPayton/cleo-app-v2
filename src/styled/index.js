import styled from 'styled-components';

import { STYLES } from '../constants';

const fontSize = ({small, large}) => {
  const { sizeS, sizeM, sizeL } = STYLES.font;
  if (small) return sizeS;
  if (large) return sizeL;
  return sizeM; 
}

export const Button = styled.button`
  cursor: pointer;
  border: none;
  outline: none;
  margin: 0;
  padding: 0;
  width: auto;
  overflow: visible;
  background: transparent;
  text-align: inherit;
  color: inherit;
  font: inherit;
  line-height: normal;
  -webkit-font-smoothing: inherit;
  -moz-osx-font-smoothing: inherit;
  -webkit-appearance: none;
`;

export const Text = styled.p`
  margin: 5px;
  padding: 0;
  font-size: ${props => fontSize(props)};
  color: ${({color, dark}) => {
    if (color) return color;
    if (dark) return STYLES.colors.charcoal;
    return '#fff';
  }}
`;

export const Span = styled.span`
  margin: 5px;
  padding: 0;
  font-size: ${props => fontSize(props)};
  color: ${({color, dark}) => {
    if (color) return color;
    if (dark) return STYLES.colors.charcoal;
    return '#fff';
  }}
`;