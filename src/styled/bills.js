import styled from 'styled-components';

// import { STYLES } from '../constants';
import { maxHeight } from './_keyframes';

export const BillsWrapper = styled.div`

`;

export const BillsRow = styled.div`
  background-color: #fff;
  padding: 5px;
  margin-top: 10px;
`;

export const Transactions = styled.div`
animation: ${maxHeight} .25s ease 1 forwards;
`;