import React from 'react';
import { shallow } from 'enzyme';

import Bills from '.';

describe('<Bills />', () => {
  let wrapper;

  it('Renders without crashing', () => {
    wrapper = shallow(<Bills />);
    expect(wrapper).toHaveLength(1);
  });
});
