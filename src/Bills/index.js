import React from 'react';
import { consumeContext } from '../context';
import { formatDate } from '../utilities';

import { Text, Span, Button } from '../styled';
import { BillsWrapper, BillsRow, Transactions } from '../styled/bills';

import { STYLES } from '../constants';

class Bills extends React.Component {
  state = {
    openedTransaction: null,
  };

  openTransactions = e => {
    const currentOpened = this.state.openedTransaction;
    let openedTransaction = e.currentTarget.id.slice(4);

    if (currentOpened === openedTransaction) openedTransaction = null;

    this.setState({openedTransaction});
  }

  moveBetweenLists = e => {
    let billId = e.currentTarget.id.slice(4);
    this.props.actions.handleBillChange(billId);
  }

  shouldHide = currentId => {
    const { category } = this.props.selected;
    return category === currentId || category === -1 ? false : true;
  }

  render() {
    const {
      type,
      actionName,
      api: {
        categories,
      },
      processed,
      selected: { category },
    } = this.props;

    const { openedTransaction } = this.state;

    return (
      <BillsWrapper>
        {categories.map(cat => {
          const matchingBills = processed[type].filter(bill => bill.categoryId === cat.id);
          
          if (!matchingBills.length && cat.id !== -1) return (
            <BillsRow key={'empty' + cat.id} hidden={category === cat.id ? false : true}>
              <Text color={STYLES.colors.green}>No {cat.name} bills!</Text>
            </BillsRow>
          );

          return matchingBills.map(({categoryId, id, name, iconUrl, transactions}) => (
            <BillsRow key={id} hidden={this.shouldHide(categoryId)}>
              <Text dark>
                {name}
                {/* {iconUrl && <Span><img src={iconUrl} alt="" style={{width: '30px', height: '30px'}} /></Span>} */}
              </Text>
              <Text small dark>{transactions.length} transaction{transactions.length !== 1 && 's'}</Text>
              <Button onClick={this.openTransactions} type="button" id={'open'+id}>
                <Span small color={STYLES.colors.red}>{id !== openedTransaction ? 'Show' : 'Hide'} transactions</Span>
              </Button>
              <Button onClick={this.moveBetweenLists} type="button" id={'move'+id}>
                <Span small color={STYLES.colors.red}>{actionName}</Span>
              </Button>
              <Transactions hidden={id !== openedTransaction}>
                {transactions.map(({amount, date, id}) => (
                  <Text key={id} small dark>{formatDate(date)}, Â£{amount}</Text>
                ))}
              </Transactions>
            </BillsRow>
          ));
        })}
      </BillsWrapper>
    )
  }
}

export default consumeContext(Bills);
